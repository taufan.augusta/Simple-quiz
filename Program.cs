﻿using System;

namespace quiz1
{
    class Program
    {
        static void Main(string[] args)
        {
            ShioCalc shioCalc = new ShioCalc();
            int day, month, year;

            Console.Write($"Name :");
            string name = Console.ReadLine();
            
            Console.Write($"Day of birth :");
            day = Convert.ToInt32(Console.ReadLine());
            Console.Write($"Month of birth :");
            month = Convert.ToInt32(Console.ReadLine());
            Console.Write($"Year of birth :");
            year = Convert.ToInt32(Console.ReadLine());

            AgeCalc ageCalc = new AgeCalc(day, month, year);
            
            Console.WriteLine(name);
            Console.WriteLine($"Age: {ageCalc.Age()}");
            int shioUser = shioCalc.GetShio(year);
            Shio shio = (Shio)shioUser;
            Console.WriteLine($"Shio: {shio}");

        }
    }
}
