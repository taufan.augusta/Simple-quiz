using System;

namespace quiz1
{
    public enum Shio
    {
        Rat,
        Ox,
        Tiger,
        Rabbit,
        Dragon,
        Snake,
        Horse,
        Goat,
        Monkey,
        Rooster,
        Dog,
        Pig
    }

    public class ShioCalc
    {
        public int[,] shioTable = new int[12,12];
       
        public void ShioGenerator()
        {
            int startingYear = 1912;
            for(int shio=0; shio < 12; shio++)            
            {
                shioTable[shio,0] = startingYear;
                int leap = startingYear;
                for (int years = 0; years < 12; years++)
                {
                    shioTable[shio,years] = leap; 
                    leap += 12;
                }
                startingYear++;
            }  
        }

        public ShioCalc()
            => ShioGenerator();

        public void ShowAllShio()
        {
            for (int i = 0; i < 12; i++)
            {
                Shio shio = (Shio)i;
                Console.Write(shio + " - ");
                for(int j = 0; j < 12; j++)
                {
                    Console.Write(shioTable[i,j] + ", ");
                }
                Console.WriteLine("");             
            }
        }

        public int GetShio(int year)
        {
            for(int shio=0; shio < 12; shio++)            
            {
                for (int years = 0; years < 12; years++)
                {
                    if(shioTable[shio, years] == year)
                    {
                        return shio;
                    }
                }
            } 
            return -1; 
        }
    }
}